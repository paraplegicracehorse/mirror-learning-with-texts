Learning with Texts (LWT) is a tool for Language Learning.

Originally created and maintained and hosted at sourceforge.net. On 2020-09-19 the code was mysteriously taken down and replaced by a single file indicating the project was under legal threat and please direct all questions to LingQ.com.

This repository was created by an installation archive taken 2020-09-17 and kindly provided to your friendly mirror host by Colin Jacobs.

A complete archive of the latest original release .zip archives, documentation, and sample database entries have been kindly provided by @language_learning_guy at https://gitlab.com/language_learning_guy/learning_with_texts.

> PLEASE READ MORE ...

> Either open ... info.htm (within the distribution)

> or     open ... http://lwt.sf.net <DEFUNCT. 2020-09-19>>

> UP-TO-DATE INSTALLATION INSTRUCTIONS can be found in the

> file "LWT_INSTALLATION.txt" !!

The best installation guide this mirror maintainer is able to locate is at https://www.mezzoguild.com/how-to-install-learning-with-texts-lwt/

> "Learning with Texts" (LWT) is free and unencumbered software released into the PUBLIC DOMAIN.

> Anyone is free to copy, modify, publish, use, compile, sell, or distribute this software, either in source code form or as a compiled binary, for any purpose, commercial or non-commercial, and by any means.

> In jurisdictions that recognize copyright laws, the author or authors of this software dedicate any and all copyright interest in the software to the public domain. We make this dedication for the benefit of the public at large and to the detriment of our heirs and successors. We intend this dedication to be an overt act of relinquishment in perpetuity of all present and future rights to this software under copyright law.

> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

> For more information, please refer to http://unlicense.org/.

For commercial alternatives with active support communities, see [LingQ](https://lingq.com) and [ReadLang](https://readlang.com).
